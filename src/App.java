import java.util.Arrays;


public class App {

	public static void main(String[] args) {
		Book[] lib = {
				new ArtBook("London, Jack", "White Fang", ArtisticGenre.DRAMA),
				new ArtBook("Conan Doyle, Arthur", "The Hound of the Baskervilles", ArtisticGenre.DETECTIVE),
				new EducationalBook("Demiovich, Boris", "The collection of problems and exercises in mathematical analysis", Subject.MATHEMATICS),
				new ScientificBook("Einshtein, Albert", "On a Heuristic Viewpoint Concerning the Production and Transformation of Light", ResearchField.FIELD_THEORY)
		};
		Arrays.sort(lib, new sortByAuthor());
		for (Book book : lib) {
			System.out.println(book);
		};
		
		System.out.println("-----------");
		
		Arrays.sort(lib, new sortByTitle());
		for (Book book : lib) {
			System.out.println(book);
		}
	}

}
