
public class ArtBook implements Book {
	
	private String author;
	private String title;
	private ArtisticGenre artisticGenre;
	
	public ArtBook(String author, String title, ArtisticGenre artisticGenre) {
		this.author = author;
		this.title = title;
		this.artisticGenre = artisticGenre;
	}

	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}
	
	public String toString() {
		return "Auhor: " + author + ". Title: " + title + ". " + artisticGenre;
	}

}
