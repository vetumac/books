
public class EducationalBook implements Book {
	private String author;
	private String title;
	private Subject subject;
	
	public EducationalBook(String author, String title, Subject subject) {
		this.author = author;
		this.title = title;
		this.subject = subject;
	}

	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}
	
	public String toString() {
		return "Auhor: " + author + ". Title: " + title + ". " + subject;
	}

}
