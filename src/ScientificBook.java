

public class ScientificBook implements Book {

	private String author;
	private String title;
	private ResearchField researchField;
	
	public ScientificBook(String author, String title, ResearchField researchField) {
		this.author = author;
		this.title = title;
		this.researchField = researchField;
	}

	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}
	
	public String toString() {
		return "Auhor: " + author + ". Title: " + title + ". " + researchField;
	}
	
}
